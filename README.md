# Text Statistics
Small application to acquire some statistics of a text file.
It is also usable as package.

## Development

### Requirements
- install Python 3.12

### Execution
- change into top level dir `text_statistics/`
- run `$ python3.12 -m text_statistics [path/to/text-file]`

### Unit Testing
- change into top level dir `text_statistics/`
- run `$ python3.12 -m unittest discover tests/`

## Installation as Package
- change into top level dir `text_statistics/`
- run `$ python3.12 -m pip install .`
- Usage:
```Python
import text_statistics as ts

ts.get_statistics("Hello\nWorld and\nbye")
```

## Containerizing
Docker is used to build an image and instantiate a container from it.

Run the following commands to build and run the app locally:
```commandline
docker build -t text_statistics .
docker run --env FILE=file1 text_statistics
```

You can also pull the last image (that passed all tests) from the gitlab container registry and run it.
```commandline
docker pull registry.gitlab.com/immrn/text-statistics:latest
docker run --env FILE=file1 registry.gitlab.com/immrn/text-statistics:latest
```