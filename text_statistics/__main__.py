from pathlib import Path
import text_statistics.stats as ts


# Calculate the statistics of a given text file
args = ts._parse_args()
with Path(args['file']).open() as f:
    text = f.read()
stats = ts.get_statistics(text)

# Present the results
print(f'Number of lines:\t{stats['lines']}')
print(f'Number of words:\t{stats['words']}')
print(f'Number of letters:\t{stats['letters']}')
print(f'Line with most words:\t{stats['line_most_words']['id']}, with {stats['line_most_words']['words']} words')
print(f'Line with most letters:\t{stats['line_most_letters']['id']}, with {stats['line_most_letters']['letters']} letters')
print(f'List of duplicated words and their counts:')
for w in sorted(stats['duplicated_words'].keys()):  # sort for deterministic presentation
    print(f'  {w}: {stats['duplicated_words'][w]}')
