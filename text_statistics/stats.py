import argparse
import os
import sys


def _parse_args(args: list[str] = None) -> dict:
    """Parse the console arguments.
    
    args -- None to use sys.args; if arg 'file' is not stated it will use the env var 'FILE'
    """
    parser = argparse.ArgumentParser(description='Process a text file and state statistics.')
    parser.add_argument(
        'file',
        type=str,
        help="path to a text file, can be set by the env var FILE.",
        nargs='?',
        default=os.getenv('FILE')
    )
    args = parser.parse_args(args)
    if not args.file:
        print('Error: no file path given (--help for more info).')
        sys.exit(1)
    return args.__dict__


def get_statistics(text: str) -> dict:
    """Calculate the statistics of a text.
    
    text -- text containing words and spaces
    """
    lines = text.splitlines()

    words = []  # all words will be collected iteratively
    count_letters = 0

    line_id_most_words = None
    count_letters_line_most_words = 0
    max_words = 0

    line_id_most_letters = None
    max_letters = 0

    for id, line in enumerate(lines):
        curr_words = line.lower().split(" ")  # lower() for better duplicate identification
        curr_words = [word for word in curr_words if word != '']
        
        curr_count_words = len(curr_words)
        curr_count_letters = sum(len(word) for word in curr_words)
        
        words += curr_words
        count_letters += curr_count_letters

        # Check if the current line has the most words
        if curr_count_words > max_words:
            line_id_most_words = id
            max_words = curr_count_words
        elif curr_count_words == max_words:
            if curr_count_letters > count_letters_line_most_words:
                line_id_most_words = id
                max_words = curr_count_words
                count_letters_line_most_words = curr_count_letters

        # Check if the current line has the most letters
        if curr_count_letters > max_letters:
            line_id_most_letters = id
            max_letters = curr_count_letters

    # Count duplicated words
    duplicated_words = {word: words.count(word) for word in set(words) if words.count(word) > 1}

    return {
        'lines': len(lines),
        'words': len(words),
        'letters': count_letters,
        'line_most_words': {
            'id': line_id_most_words,
            'words': max_words
        },
        'line_most_letters': {
            'id': line_id_most_letters,
            'letters': max_letters
        },
        'duplicated_words': duplicated_words
    }
