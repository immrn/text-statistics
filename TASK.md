# The Word Counter

Create an application for processing text files to calculate statistics.

## About this Test

You have a one week time limit for this task. Submitting an incomplete solution is not an automatic failure.
Handle this task as you would any task at work.

If there are any questions, contact us at [cdh@secunet.com](mailto:cdh@secunet.com).

## The Task

### Step 1: Processing Words

Create a python application that loads a text file from a given location.

The application shall be able to read the file location from a command line argument and from an environment variable.

The file will contain words ([A-Za-z]+), spaces and new-lines.

Process the file to calculate and output the follow statistics:

* The number of lines
* The number of words
* The number of letters
* The line number and word count for the line with the most words (on a tie, the line with the most letters wins)
* The line number and letter count for the longest line
* A list with all words that appear multiple times and their count

### Step 2: Containerizing

Package the application into a container image.

When the container image starts, it shall call the application.

Document the build and usage of the container.

### Step 3: Gitlab CI

Write a gitlab-ci file that builds the container image and pushes it into a (gitlab) container registry.

## Submit the Solution

When finished, submit the solution as a packaged project to [cdh@secunet.com](mailto:cdh@secunet.com)
