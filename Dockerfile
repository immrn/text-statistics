FROM python:3.12.3-slim

RUN mkdir -p /app
WORKDIR /app

# If we had dependecies, we could upgrade pip and install
# the dependecies here. No need at the moment.

COPY . ./
CMD ["python3.12", "-m", "text_statistics"]