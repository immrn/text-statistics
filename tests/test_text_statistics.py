import unittest
import text_statistics.stats as ts
import contextlib
import io


class TestParser(unittest.TestCase):

    def test_file(self):
        expected = {'file': 'file1'}
        result = ts._parse_args(['file1'])
        self.assertDictEqual(result, expected)

    def test_file_not_stated(self):
        with self.assertRaises(SystemExit):
            # Suppress error prints because we are testing for errors
            with contextlib.redirect_stdout(io.StringIO()):
                ts._parse_args([''])


class TestTextStatistics(unittest.TestCase):

    def test_get_statistics(self):
        input ='Aa bbb cccc\n' \
            'aaa bbb ccc ddd eee\n' \
            'Aaaaa bbbbb ccccc ddddd eeeee\n' \
            'aa bbb cc dd ee\n' \
            'Aaaaaaaaaaaaaaaaaaaa bbbbbbbbbbbbbb\n' \
            'a b cc d'
        
        expected = {
            'lines': 6,
            'words': 24,
            'letters': 99,
            'line_most_words': {
                'id': 2,
                'words': 5
            },
            'line_most_letters': {
                'id': 4,
                'letters': 34
            },
            'duplicated_words': {'aa': 2, 'bbb': 3, 'cc': 2}
        }

        result = ts.get_statistics(input)
        self.assertDictEqual(result, expected)

    
    def test_get_statistics_empty_inputs(self):
        def base_expected():
            return {
                'lines': 1,
                'words': 0,
                'letters': 0,
                'line_most_words': {
                    'id': None,
                    'words': 0
                },
                'line_most_letters': {
                    'id': None,
                    'letters': 0
                },
                'duplicated_words': {}
            }

        # Empty str
        result = ts.get_statistics('')
        expected = base_expected()
        expected['lines'] = 0
        self.assertDictEqual(result, expected, msg='empty str')

        # Space
        result = ts.get_statistics(' ')
        expected = base_expected()
        self.assertDictEqual(result, expected, msg='space')

        result = ts.get_statistics('  ')
        expected = base_expected()
        self.assertDictEqual(result, expected, msg='2 spaces')

        # line break
        result = ts.get_statistics('\n')
        expected = base_expected()
        self.assertDictEqual(result, expected, msg='line break')

        result = ts.get_statistics('\n\n')
        expected = base_expected()
        expected['lines'] = 2
        self.assertDictEqual(result, expected, msg='2 line breaks')


if __name__ == '__main__':
    unittest.main()